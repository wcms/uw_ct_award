/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * This js file allows content authors/editors to select taxonomy terms easily when creating award form.
 */

(function ($) {
   $(function () {

      // Page load when 'Undergraduate: Open to Any Program' is checked, hide others.
    if ($('#edit-field-undergrad-award-program-und input').first().is(':checked')) {
      $('#edit-field-undergrad-award-program-und li:not(:first)').hide().find('input').removeAttr('checked');
    }
    else {
      $('#edit-field-undergrad-award-program-und li').show();
    }

      // Check 'Undergraduate: Open to Any Program' to hide program tree, uncheck to show.
      $('#edit-field-undergrad-award-program-und input').first().click(function () {
        if ($('#edit-field-undergrad-award-program-und input').first().is(':checked')) {
          $('#edit-field-undergrad-award-program-und li:not(:first)').hide().find('input').removeAttr('checked');
        }
        else {
          $('#edit-field-undergrad-award-program-und li').show();
        }
      });

      // Page load when 'Graduate: Open to Any Program' is checked, hide others.
      if ($('#edit-field-undergrad-award-program2-und input').first().is(':checked')) {
        $('#edit-field-undergrad-award-program2-und li:not(:first)').hide().find('input').removeAttr('checked');
      }
      else {
        $('#edit-field-undergrad-award-program2-und li').show();
      }

      // Check 'Graduate: Open to Any Program' to hide program tree, uncheck to show.
      $('#edit-field-undergrad-award-program2-und input').first().click(function () {
        if ($('#edit-field-undergrad-award-program2-und input').first().is(':checked')) {
          $('#edit-field-undergrad-award-program2-und li:not(:first)').hide().find('input').removeAttr('checked');
        }
        else {
          $('#edit-field-undergrad-award-program2-und li').show();
        }
      });

      // Create parent terms "select all" and "deselect all" link-like spans for Undergraduate.
      $('#edit-field-undergrad-award-program-und .parent-term').append(' <span class="clickable-js select-all" title="select all"></span> <span class="clickable-js deselect-all" title="deselect all"></span>');
      // When a "select all" link-like span is clicked, check all the items under this term and expand the tree if needed.
      $('#edit-field-undergrad-award-program-und .parent-term .select-all').click(function () {
         $('input',$(this).parent().next('ul')).attr('checked', true);
         $('.term-reference-tree-collapsed',$(this).parents('li')).click();
      });
      // When a "deselect all" link-like span is clicked, uncheck all the items under this term and expand the tree if needed for Undergraduate.
      $('#edit-field-undergrad-award-program-und .parent-term .deselect-all').click(function () {
         $('input',$(this).parent().next('ul')).attr('checked',false);
         $('.term-reference-tree-collapsed',$(this).parents('li')).click();
      });

      // Create parent terms "select all" and "deselect all" link-like spans for Graduate.
      $('#edit-field-undergrad-award-program2-und .parent-term').append(' <span class="clickable-js select-all" title="select all"></span> <span class="clickable-js deselect-all" title="deselect all"></span>');
      // When a "select all" link-like span is clicked, check all the items under this term and expand the tree if needed.
      $('#edit-field-undergrad-award-program2-und .parent-term .select-all').click(function () {
         $('input',$(this).parent().next('ul')).attr('checked', true);
         $('.term-reference-tree-collapsed',$(this).parents('li')).click();
      });
      // When a "deselect all" link-like span is clicked, uncheck all the items under this term and expand the tree if needed for Graduate.
      $('#edit-field-undergrad-award-program2-und .parent-term .deselect-all').click(function () {
         $('input',$(this).parent().next('ul')).attr('checked', false);
         $('.term-reference-tree-collapsed',$(this).parents('li')).click();
      });

      // Create a "Select all award types".
      /*$('#edit-field-undergrad-award-type-und').prepend('<div id="select-all-types"><input class="form-checkbox" type="checkbox" name="types" value="Select all types"  /> Select all types</div>');
      $('#select-all-types').click(function() {
         if($('input:checkbox[name=types]').is(':checked')){
           $('#edit-field-undergrad-award-type-und input').attr('checked',true);
         }
         else {
           $('#edit-field-undergrad-award-type-und input').attr('checked',false);
         }
      });*/

      // Create a "Select all citizenship status".
      $('#edit-field-undergrad-award-citizen-und').prepend('<div id="select-all-status"><input class="form-checkbox" type="checkbox" name="status" value="Select all status"  /> Select all statuses</div>');
      $('#select-all-status').click(function () {
        if ($('input:checkbox[name=status]').is(':checked')) {
          $('#edit-field-undergrad-award-citizen-und input').attr('checked', true);
        }
        else {
          $('#edit-field-undergrad-award-citizen-und input').attr('checked', false);
        }
      });

      // Create a "Select all enrollment year".
      $('#edit-field-undregrad-award-enroll-und').prepend('<div id="select-all-years"><input class="form-checkbox" type="checkbox" name="years" value="Select all years"  /> Select all years</div>');
      $('#select-all-years').click(function () {
        if ($('input:checkbox[name=years]').is(':checked')) {
          $('#edit-field-undregrad-award-enroll-und input').attr('checked', true);
        }
        else {
          $('#edit-field-undregrad-award-enroll-und input').attr('checked', false);
        }
      });

      // Create a "Application deadline".
      $('#edit-field-undergrad-award-deadline-und').prepend('<div id="deadline-any"><input class="form-checkbox" type="checkbox" name="deadline" value="Any"  />Select all deadlines</div>');
      $('#deadline-any').click(function () {
        if ($('input:checkbox[name=deadline]').is(':checked')) {
          $('#edit-field-undergrad-award-deadline-und input').attr('checked', true);
        }
        else {
          $('#edit-field-undergrad-award-deadline-und input').attr('checked', false);
        }
      });
   });
})(jQuery);
