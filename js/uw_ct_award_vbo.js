/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * This js file allows site managers to do mass public.
 */

(function ($) {
  Drupal.behaviors.uw_ct_award = {
    attach: function (context, settings) {
      $("#edit-operation").find("option:contains('- Choose an operation -')").remove();
      $("#edit-state").find("option:contains('Draft')").remove();
      $("#edit-state").find("option:contains('Needs Review')").remove();
      $("#edit-state").find("option:contains('Archived')").remove();
      $("#edit-force-transition").hide();
    }
  };
})(jQuery);
