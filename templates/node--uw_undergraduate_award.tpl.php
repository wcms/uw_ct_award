<?php

/**
 * @file
 * Template for undergraduate award node.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <?php print $user_picture; ?>

    <?php if ($display_submitted): ?>
      <div class="submitted"><?php print $date; ?> - <?php print $name; ?></div>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
        // The below is to print 8 fields.
        print render($content['field_undergrad_award_status']);
        print render($content['field_undergrad_award_type']);
        print render($content['field_undergrad_award_aff']);
        print render($content['field_undergrad_award_desc']);
        print render($content['field_undergrad_award_value']);
        print render($content['field_undergrad_award_valuedesc']);
        print render($content['field_undergrad_award_elig']);
        print render($content['field_undregrad_award_enroll']);

        // This is to print the label 'Program' for program field
        // and program field which is not print above.
        if (render($content['field_undergrad_award_program']) != "" || render($content['field_undergrad_award_program2']) != ""):
          print '<div class="field"><div class="field-label">Program:</div></div>';
        endif;

        print render($content['field_undergrad_award_program']);
        print render($content['field_undergrad_award_program2']);

        // Hide the below 8 fields, will print 6 fields one by one,
        // two fields (field_undergrad_award_gram_type and
        // field_undergrad_award_add_except) never display.
        hide($content['field_undergrad_award_gram_type']);
        hide($content['field_undergrad_award_detail']);
        hide($content['field_undergrad_award_deadline']);
        hide($content['field_undergrad_award_add_except']);
        hide($content['field_undergrad_award_except']);
        hide($content['field_undergrad_award_spec']);
        hide($content['field_undergrad_award_cont_term']);
        hide($content['field_undergrad_award_contact']);
        hide($content['field_undergrad_award_notes']);

        // The below is for print citizenship (field_undergrad_award_citizen),
        // selection process (field_undergrad_award_ptype) and
        // term (field_undergard_award_term) fields.
        print render($content);

        // This is to print the label 'Application details' for detail field
        // and print detail field which is hidden above.
        $application_details = render($content['field_undergrad_award_detail']);
        if ($application_details != ""):
          print '<div class="field"><div class="field-label">Application details:</div></div>' . $application_details;
        endif;

        // Print the below five fields which are hidden above.
        print render($content['field_undergrad_award_deadline']);
        print render($content['field_undergrad_award_except']);
        print render($content['field_undergrad_award_spec']);

        // This is to print the label 'Contact' for contact field
        // and print contact field which is hidden above.
        $award_contact = render($content['field_undergrad_award_cont_term']);
        if ($award_contact != ""):
          print '<div class="field"><div class="field-label">Contact:</div></div>' . $award_contact;
        endif;

        print render($content['field_undergrad_award_contact']);
        print render($content['field_undergrad_award_notes']);
       ?>

       <!-- when url includes 'search-results?level=', it displays the link Return to Results. -->
       <?php $pos = strpos($_SERVER['HTTP_REFERER'], 'search-results?level='); ?>
       <?php if ($pos !== FALSE): ?>
         <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">Return to Results</a>
       <?php endif;?>
    </div>
    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>
  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>
<?php print render($content['service_links']); ?>
