<?php

/**
 * Contains tests for uw_ct_award.
 */
class UwCtAwardTestCase extends UwWcmsTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => t('Award'),
      'description' => t('Test uw_ct_award.'),
      'group' => t('UW WCMS'),
      'mode' => 'site',
    ];
  }

  /**
   * Test who can view/edit Award search result header.
   *
   * Only "site manager" or "administrator" are allowed.
   */
  public function test() {
    module_enable(['uw_ct_award']);
    $path = 'admin/config/system/award_search_result_header';

    foreach ([
      'award content author',
      'content author',
      'content editor',
      'form editor',
    ] as $user) {
      if ($this->loggedInUser !== $this->users[$user]) {
        $this->drupalLogin($this->users[$user]);
      }
      $this->drupalGet($path);
      $this->assertResponse(403, $user . ' user is not able to access ' . $path . '.');
    }

    foreach ([
      'site manager',
      'administrator',
      'WCMS support',
    ] as $user) {
      if ($this->loggedInUser !== $this->users[$user]) {
        $this->drupalLogin($this->users[$user]);
      }
      $this->drupalGet($path);
      $this->assertResponse(200, $user . ' user is able to access ' . $path . '.');
    }
  }

}
