<?php

/**
 * @file
 * uw_ct_award.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_award_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_undergrad_award_landing_page';
  $context->description = 'Displays search on award landing page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'awards/database' => 'awards/database',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-3c9c7b4e493ec9bd24bf4a1380bb32ce' => array(
          'module' => 'views',
          'delta' => '3c9c7b4e493ec9bd24bf4a1380bb32ce',
          'region' => 'content',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays search on award landing page');
  $export['uw_undergrad_award_landing_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_undergrad_award_search_block';
  $context->description = 'Displays awards search on the sidebar';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'awards/search-results' => 'awards/search-results',
        'awards/search-results*' => 'awards/search-results*',
        '~awards/database' => '~awards/database',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-3c9c7b4e493ec9bd24bf4a1380bb32ce' => array(
          'module' => 'views',
          'delta' => '3c9c7b4e493ec9bd24bf4a1380bb32ce',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays awards search on the sidebar');
  $export['uw_undergrad_award_search_block'] = $context;

  return $export;
}
