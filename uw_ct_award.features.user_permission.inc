<?php

/**
 * @file
 * uw_ct_award.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_award_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access report and mass publish'.
  $permissions['access report and mass publish'] = array(
    'name' => 'access report and mass publish',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_ct_award',
  );

  // Exported permission: 'awards site type settings'.
  $permissions['awards site type settings'] = array(
    'name' => 'awards site type settings',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
    ),
    'module' => 'uw_ct_award',
  );

  // Exported permission: 'create field_undergrad_award_notes'.
  $permissions['create field_undergrad_award_notes'] = array(
    'name' => 'create field_undergrad_award_notes',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_undergrad_award_status'.
  $permissions['create field_undergrad_award_status'] = array(
    'name' => 'create field_undergrad_award_status',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create uw_undergraduate_award content'.
  $permissions['create uw_undergraduate_award content'] = array(
    'name' => 'create uw_undergraduate_award content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_undergraduate_award content'.
  $permissions['delete any uw_undergraduate_award content'] = array(
    'name' => 'delete any uw_undergraduate_award content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_undergraduate_award content'.
  $permissions['delete own uw_undergraduate_award content'] = array(
    'name' => 'delete own uw_undergraduate_award content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_undergraduate_award content'.
  $permissions['edit any uw_undergraduate_award content'] = array(
    'name' => 'edit any uw_undergraduate_award content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_undergrad_award_notes'.
  $permissions['edit field_undergrad_award_notes'] = array(
    'name' => 'edit field_undergrad_award_notes',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_undergrad_award_status'.
  $permissions['edit field_undergrad_award_status'] = array(
    'name' => 'edit field_undergrad_award_status',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_undergrad_award_notes'.
  $permissions['edit own field_undergrad_award_notes'] = array(
    'name' => 'edit own field_undergrad_award_notes',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_undergrad_award_status'.
  $permissions['edit own field_undergrad_award_status'] = array(
    'name' => 'edit own field_undergrad_award_status',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own uw_undergraduate_award content'.
  $permissions['edit own uw_undergraduate_award content'] = array(
    'name' => 'edit own uw_undergraduate_award content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_undergraduate_award revision log entry'.
  $permissions['enter uw_undergraduate_award revision log entry'] = array(
    'name' => 'enter uw_undergraduate_award revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_undergraduate_award authored by option'.
  $permissions['override uw_undergraduate_award authored by option'] = array(
    'name' => 'override uw_undergraduate_award authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_undergraduate_award authored on option'.
  $permissions['override uw_undergraduate_award authored on option'] = array(
    'name' => 'override uw_undergraduate_award authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_undergraduate_award promote to front page option'.
  $permissions['override uw_undergraduate_award promote to front page option'] = array(
    'name' => 'override uw_undergraduate_award promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_undergraduate_award published option'.
  $permissions['override uw_undergraduate_award published option'] = array(
    'name' => 'override uw_undergraduate_award published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_undergraduate_award revision option'.
  $permissions['override uw_undergraduate_award revision option'] = array(
    'name' => 'override uw_undergraduate_award revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_undergraduate_award sticky option'.
  $permissions['override uw_undergraduate_award sticky option'] = array(
    'name' => 'override uw_undergraduate_award sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'view field_undergrad_award_notes'.
  $permissions['view field_undergrad_award_notes'] = array(
    'name' => 'view field_undergrad_award_notes',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_undergrad_award_status'.
  $permissions['view field_undergrad_award_status'] = array(
    'name' => 'view field_undergrad_award_status',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'WCMS web services' => 'WCMS web services',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_undergrad_award_notes'.
  $permissions['view own field_undergrad_award_notes'] = array(
    'name' => 'view own field_undergrad_award_notes',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_undergrad_award_status'.
  $permissions['view own field_undergrad_award_status'] = array(
    'name' => 'view own field_undergrad_award_status',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
