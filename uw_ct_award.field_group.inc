<?php

/**
 * @file
 * uw_ct_award.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_award_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_undergrad_award_contact|node|uw_undergraduate_award|form';
  $field_group->group_name = 'group_undergrad_award_contact';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_undergraduate_award';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact information',
    'weight' => '20',
    'children' => array(
      0 => 'field_undergrad_award_cont_term',
      1 => 'field_undergrad_award_contact',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-undergrad-award-contact field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_undergrad_award_contact|node|uw_undergraduate_award|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact information');

  return $field_groups;
}
