<?php

/**
 * @file
 * Install, update and uninstall functions for the uw_ct_award module.
 */

/**
 * The helper function to create child taxonomy terms.
 */
function _uw_ct_award_create_term($vocab_machine_name, $taxonomy_name, $weight, $parent_tid = NULL) {
  $tid = FALSE;
  $tid = _get_award_tid_from_term_name($taxonomy_name, $vocab_machine_name);
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocab_machine_name);

  if (!$tid) {
    $term = array(
      'name' => $taxonomy_name,
      'vid' => $vocabulary->vid,
      'weight' => $weight,
      'parent' => $parent_tid,
      'vocabulary_machine_name' => $vocab_machine_name,
    );
    $term_object = (object) $term;
    taxonomy_term_save($term_object);
  }
  else {
    $term_object = taxonomy_term_load($tid);
    taxonomy_term_save($term_object);
    $message = 'term ' . $taxonomy_name . 'already exists';
    watchdog('uw_ct_award', $message);
  }
}

/**
 * Returns an array with data for default pages.
 *
 * @param string $page
 *   The page data to return.
 */
function _uw_ct_award_get_page_data($page) {
  switch ($page) {
    case 'awards':
      return array(
        'title' => 'Database',
        'content' => 'This is your <em>award</em> landing page.',
        'alias' => 'awards/database',
        'weight' => 0,
      );
  }

  // No cases matched.
  return NULL;
}

/**
 * The helper function to create a landing page.
 *
 * The alias is awards/database when enabling award content type.
 */
function _uw_ct_award_create_landing_page() {

  $path = ('awards/database');
  $path_exist = drupal_lookup_path('alias', $path) || drupal_lookup_path('source', $path);

  if (empty($path_exist)) {
    watchdog('uw_ct_award', 'awards');
    $data = _uw_ct_award_get_page_data('awards');

    // Create the node.
    $node = new stdClass();
    $node->type = 'uw_web_page';
    node_object_prepare($node);
    $node->language = $GLOBALS['language_content']->language;

    // Published - yes.
    $node->status = 1;
    $node->title = $data['title'];
    $node->body[$node->language][0]['value'] = $data['content'];
    $node->body[$node->language][0]['format'] = 'uw_tf_standard';
    $node->path['alias'] = $data['alias'];
    $node->path['pathauto'] = 0;

    node_save($node);

    // Setup a menu item.
    $item = array(
      'link_title' => st($data['title']),
      'link_path' => drupal_get_normal_path($data['alias']),
      'menu_name' => 'main-menu',
      'weight' => $data['weight'],
    );
    menu_link_save($item);
  }
}

/**
 * The helper funtion to get taxonomy term id from term name.
 */
function _get_award_tid_from_term_name($term_name, $vocabulary_name) {
  if ($vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name)) {
    $tree = taxonomy_get_tree($vocabulary->vid);
    foreach ($tree as $term) {
      if ($term->name == $term_name) {
        return $term->tid;
      }
    }
  }
  return FALSE;
}

/**
 * The helper function to create award taxonomy vocabularies.
 */
function _create_award_taxonomy_vocab() {

  // Array of machine names of vocabularies related to award content type.
  $vocabs = array(
    'undergrad_award_affiliation' => 'Award affiliation',
    'undergrad_award_citizenship' => 'Award citizenship',
    'undergrad_award_contact' => 'Award contact',
    'undergrad_award_deadline' => 'Award deadline',
    'undergrad_award_detail' => 'Award detail',
    'undergrad_award_enrollment' => 'Award enrollment',
    'undergrad_award_process' => 'Award process',
    'undergrad_award_program' => 'Award program',
    'undergrad_award_term' => 'Award term',
    'undergrad_award_type' => 'Award type',
    'uw_departments' => 'Departments',
  );

  // Loop through to create each vocabulary programmatically.
  foreach ($vocabs as $key => $item) {
    $vid = db_select("taxonomy_vocabulary", "tv")
      ->fields("tv", array("vid"))
      ->condition("machine_name", $key)
      ->execute()
      ->fetchField();
    if (!$vid) {
      taxonomy_vocabulary_save((object) array(
        'name' => $item,
        'machine_name' => $key,
      ));
    }
  }
}

/**
 * The helper function to delete menu link when the module is disabled.
 */
function _uw_ct_award_delete_menu_link($menu_name, $menu_link_path) {
  $links = FALSE;
  $links = menu_load_links($menu_name);
  $mild = FALSE;
  if (isset($links)) {
    foreach ($links as $item) {
      if ($item['menu_name'] == $menu_name && $item['link_path'] == $menu_link_path) {
        $mild = $item['mlid'];
        break;
      }
    }
    if ($mild) {
      menu_link_delete($mild);
    }
  }
}

/**
 * Implements hook_install().
 *
 * For student-awards-financial-aid site:
 * Update the existing taxonomy term name.
 * Add the new taxonomy terms.
 * Update undergraduate-awards/[node:title] to awards/[node:title].
 * Update undergraduate-awards/database to awards/database.
 * Remove the below link items under Dashboard->Vocabularies.
 */
function uw_ct_award_install() {
  // Create a landing page (award/database) if there is no landing page.
  _uw_ct_award_create_landing_page();

  // Do one variable_set for every content type using workbench moderation.
  variable_set('node_options_uw_ct_award', array(0 => 'moderation', 1 => 'revision'));

  // Create undergraduate award taxonomy vocabulary.
  _create_award_taxonomy_vocab();

  watchdog('uw_ct_award', 'install hook executing');

  // All taxonomy terms except undergrad_award_program and uw_departments.
  $taxonomy = [
    'undergrad_award_affiliation' => [
      'Student athlete',
      'Mature learner',
      'Ontario first generation',
      'Part time learner',
      'Aboriginal',
    ],
    'undergrad_award_type' => [
      'Athletic awards',
      'Entrepreneurial awards',
      'Financial need awards/bursaries',
      'EInternational experience awards',
      'Research awards',
      'Scholarships/awards',
    ],
    'undergrad_award_enrollment' => [
      'Entering First Year Undergraduate Studies',
      'Undergraduate Year One',
      'Undergraduate Year Two',
      'Undergraduate Year Three',
      'Undergraduate Year Four',
      'Masters',
      'Doctoral',
    ],
    'undergrad_award_citizenship' => [
      'Canadian/Permanent resident',
      'International/study permit student',
    ],
    'undergrad_award_process' => [
      'Application required',
      'Student selected automatically by Faculty/Department',
    ],
    'undergrad_award_detail' => [
      'Bursary/award application required',
      'General UG Award Application required',
      'International Experience Award Application required',
      'Letter explaining how you meet the award criteria',
      'Resume required',
      'Supporting documentation as indicated on application form',
    ],
    'undergrad_award_deadline' => [
      'January 15',
      'February 1',
      'February 15',
      'March 1',
      'March 15',
      'June 1',
      'June 15',
      'July 1',
      'July 15',
      'September 15',
      'October 1',
      'October 15',
      'November 1',
      'November 15',
      'Varies',
    ],
    'undergrad_award_term' => [
      'Winter',
      'Spring',
      'Fall',
    ],
  ];

  // Loop the nested array to create taxonomy term for the above vocabs.
  foreach ($taxonomy as $vocab => $t) {
    foreach ($t as $weight => $term) {
      _uw_ct_award_create_term($vocab, $term, $weight);
    }
  }

  // All the taxonomy terms under undergrad_award_program into a nested array.
  $undergrad_award_program_taxonomy = [
    'Open to any program' => [],
    'Health' => [
      'Kinesiology',
      'Public Health and Health Systems',
      'Recreation and Leisure Studies',
    ],
    'Arts' => [
      'Accounting and Financial Mgmt',
      'Anthropology',
      'Recreation and Leisure Studies',
      'Classical Studies',
      'Computing and Financial Mgmt',
      'Drama and Speech Communication',
      'Economics',
      'English Language and Literature',
      'Fine Arts',
      'French Studies',
      'Germanic and Slavic Studies',
      'Global Business and Digital Arts',
      'History',
      'Independent Studies',
      'Medieval Studies',
      'Music',
      'Peace and Conflict Studies',
      'Philosophy',
      'Political Science',
      'Psychology',
      'Religious Studies',
      'Russian and European Studies',
      'Sexuality, Marriage and Family Studies',
      'Social Development Studies',
      'Sociology and Legal Studies',
      'Spanish and Latin American Studies',
      'Women\'s Studies',
    ],
    'Engineering' => [
      'Biomedical Engineering',
      'Chemical Engineering',
      'Civil Engineering',
      'Computer Engineering',
      'Electrical Engineering',
      'Environmental Engineering',
      'Geological Engineering',
      'Management Sciences',
      'Mechanical Engineering',
      'Mechatronics Engineering',
      'Nanotechnology Engineering',
      'Software Engineering',
      'Systems Design Engineering',
    ],
    'Environment' => [
      'Environment and Business',
      'Environment and Resource Studies',
      'Geography and Environmental Mgmt',
      'Geography and Aviation',
      'Geomatics',
      'International Development',
      'Knowledge Integration',
      'Planning',
    ],
    'Mathematics' => [
      'Actuarial Science',
      'Applied Mathematics',
      'Bioinformatics',
      'Business and CS (Double Degree)',
      'Business and Math(Double Degree)',
      'Combinatorics and Optimization',
      'Computer Science',
      'Computing and Financial Mgmt',
      'Information Technology Mgmt',
      'Math/Fin.Analysis and Risk Mgmt',
      'Mathematical Economics',
      'Mathematical Finance',
      'Mathematical Optimization',
      'Mathematical Physics',
      'Mathematics',
      'Mathematics/Business Admin',
      'Mathematics/CPA',
      'Mathematics/Teachers',
      'Mathematics/Teaching',
      'Pure Mathematics',
      'Scientific Computation/Applied Mathematics',
      'Statistics',
    ],
    'Science' => [
      'Biology',
      'Biotechnology/CPA',
      'Biotechnology/Economics',
      'Chemistry',
      'Earth and Environmental Sciences',
      'Optometry',
      'Pharmacy',
      'Physics and Astronomy',
      'Science and Business',
      'Science and Aviation',
    ],
  ];

  // Loop the nested array to create taxonomy terms for undergrad_award_program.
  $parent_weight = 0;
  foreach ($undergrad_award_program_taxonomy as $parent => $t) {
    _uw_ct_award_create_term('undergrad_award_program', $parent, $parent_weight);
    $parent_tid = _get_award_tid_from_term_name($parent, 'undergrad_award_program');
    if (!empty($t)) {
      foreach ($t as $weight => $term) {
        _uw_ct_award_create_term('undergrad_award_program', $term, $weight, $parent_tid);
      }
    }
    $parent_weight++;
  }

  // All the taxonomy terms under uw_departments into a nested array.
  $uw_departments_taxonomy = [
    'Open to any program' => [],
    'Health' => [
      'Health Studies/Gerontology',
      'Kinesiology',
      'Public Health',
      'Recreation and Leisure Studies',
    ],
    'Arts' => [
      'Accounting',
      'Anthropology (Public issues)',
      'Classical Studies',
      'Digital Experience Innovation',
      'Economics',
      'English',
      'Fine Arts',
      'French Studies',
      'German/Russian',
      'Global Governance',
      'History',
      'Peace & Conflict Studies',
      'Philosophy',
      'Political Science',
      'Psychology',
      'Religious Studies',
      'Sociology and Legal Studies',
      'Social Development Studies',
      'Social Work, Arts',
      'Taxation',
    ],
    'Engineering' => [
      'Architecture',
      'MBET',
      'Chemical Engineering',
      'Civil & Environmental Eng',
      'Electrical & Computer Eng',
      'Management Sciences',
      'Mechanical & Mechatronics Eng',
      'Quantum Computing',
      'Systems Design Engineering',
    ],
    'Environment' => [
      'Environment & Business',
      'Environment & Resource Studies',
      'Geography & Environmental Mgmt',
      'Planning',
      'Social Innovation',
    ],
    'Mathematics' => [
      'Applied Mathematics',
      'Combinatorics & Optimization',
      'Computational Mathematics',
      'Computer Science',
      'Mathematics for Teacher',
      'Pure Mathematics',
      'Quantitative Finance',
      'Statistics & Actuarial Science',
    ],
    'Science' => [
      'Biology',
      'Chemistry',
      'Earth & Environmental Sciences',
      'Pharmacy',
      'Physics',
      'Physics and Astronomy',
      'Vision Science',
    ],
    'Theology' => [
      'Catholic Thought',
      'Theological Studies',
    ],
  ];

  // Loop the nested array to create taxonomy terms for uw_departments.
  $parent_weight = 0;
  foreach ($uw_departments_taxonomy as $parent => $t) {
    _uw_ct_award_create_term('uw_departments', $parent, $parent_weight);
    $parent_tid = _get_award_tid_from_term_name($parent, 'uw_departments');
    if (!empty($t)) {
      foreach ($t as $weight => $term) {
        _uw_ct_award_create_term('uw_departments', $term, $weight, $parent_tid);
      }
    }
    $parent_weight++;
  }

  // Update graduate-awards/[node:title] to awards/[node:title].
  $pid = db_query("SELECT MAX(pid) FROM {url_alias}")->fetchField();
  $result = db_query("SELECT nid FROM {node} WHERE type = 'uw_student_award'");
  foreach ($result as $record) {
    $source = 'node/' . $record->nid;
    $query = "SELECT pid, alias FROM {url_alias} where source = :source";
    $args = array(
      ':source' => $source,
    );
    $result = db_query($query, $args);
    foreach ($result as $record) {
      $alias_array = explode("/", $record->alias);
      $alias_new = 'awards/' . $alias_array[1];
      $pid = $pid + 1;
      $data = array(
        'pid' => $pid,
        'source' => $source,
        'alias' => $alias_new,
        'language' => 'en',
      );
      drupal_write_record('url_alias', $data);
      db_delete('url_alias')->condition('pid', $record->pid)->execute();
    }
  }

  // Update awards-funding/database/graduate-funding-and-awards-database
  // to awards/database for the existing GSO site.
  $pid = db_query("SELECT MAX(pid) FROM {url_alias}")->fetchField();
  $result = db_query("SELECT pid, source FROM {url_alias} where alias = 'awards-funding/database/graduate-funding-and-awards-database'");
  foreach ($result as $record) {
    $pid = $pid + 1;
    $data = array(
      'pid' => $pid,
      'source' => $record->source,
      'alias' => 'awards/database',
      'language' => 'en',
    );
    drupal_write_record('url_alias', $data);
    db_delete('url_alias')->condition('pid', $record->pid)->execute();
  }

  // Update undergraduate-awards/[node:title] to awards/[node:title].
  $pid = db_query("SELECT MAX(pid) FROM {url_alias}")->fetchField();
  $result = db_query("SELECT nid FROM {node} WHERE type = 'uw_undergraduate_award'");
  foreach ($result as $record) {
    $source = 'node/' . $record->nid;
    $query = "SELECT pid, alias FROM {url_alias} where source = :source";
    $args = array(
      ':source' => $source,
    );
    $result = db_query($query, $args);
    foreach ($result as $record) {
      $alias_array = explode("/", $record->alias);
      $alias_new = 'awards/' . $alias_array[1];
      $pid = $pid + 1;
      $data = array(
        'pid' => $pid,
        'source' => $source,
        'alias' => $alias_new,
        'language' => 'en',
      );
      drupal_write_record('url_alias', $data);
      db_delete('url_alias')->condition('pid', $record->pid)->execute();
    }
  }

  // Update undergraduate-awards/database to awards/database.
  $pid = db_query("SELECT MAX(pid) FROM {url_alias}")->fetchField();
  $result = db_query("SELECT pid, source FROM {url_alias} where alias = 'undergraduate-awards/database'");
  foreach ($result as $record) {
    $pid = $pid + 1;
    $data = array(
      'pid' => $pid,
      'source' => $record->source,
      'alias' => 'awards/database',
      'language' => 'en',
    );
    drupal_write_record('url_alias', $data);
    db_delete('url_alias')->condition('pid', $record->pid)->execute();
  }
}

/**
 * Implements hook_uninstall().
 */
function uw_ct_award_uninstall() {
  // Remove the RDF mapping from the database.
  rdf_mapping_delete('node', 'uw_ct_award');

  // Remove the variable that we set.
  variable_del('node_options_uw_ct_award');

  // Array of machine names of vocabularies related to award content type.
  $vocabs = array(
    'undergrad_award_type',
    'undergrad_award_enrollment',
    'undergrad_award_citizenship',
    'undergrad_award_program',
    'undergrad_award_process',
    'undergrad_award_detail',
    'undergrad_award_deadline',
    'undergrad_award_term',
    'undergrad_award_affiliation',
    'uw_departments',
    'undergrad_award_contact',
  );

  // Remove taxonomy vocabulary.
  foreach ($vocabs as $item) {
    $vocab = taxonomy_vocabulary_machine_name_load($item);
    if ($vocab) {
      taxonomy_vocabulary_delete($vocab->vid);
    }
  }
}

/**
 * Implements hook_disable().
 *
 * Disable the views and remove menu links related when disable module.
 */
function uw_ct_award_disable() {
  watchdog('uw_ct_award', 'disable function executing');

  // Disable the view.
  // Read in any existing disabled views.
  $views_status = variable_get('views_defaults', array());

  // Add ours to the list.
  $views_status['manage_undergrad_awards'] = TRUE;
  $views_status['uw_undergrad_award_mass_publish'] = TRUE;
  $views_status['uw_undergrad_award_page'] = TRUE;
  $views_status['uw_undergrad_award_report'] = TRUE;
  $views_status['uw_undergrad_award_search_block'] = TRUE;
  $views_status['uwaterloo_services_undergrad_awards'] = TRUE;

  // Reset the variable with the new list.
  variable_set('views_defaults', $views_status);
  if (function_exists('views_invalidate_cache')) {
    // Clear the views cache.
    views_invalidate_cache();
  }

  // Remove Award search result header menu link under Site management
  // when uw_ct_award module is disabled.
  _uw_ct_award_delete_menu_link('menu-site-management', 'admin/config/system/award_search_result_header');

  // Remove Award vocab menu links under Vocabularies
  // when uw_ct_award module is disabled.
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_affiliation');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_citizenship');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_contact');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_deadline');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_detail');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_enrollment');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_process');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_program');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_term');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/undergrad_award_type');
  _uw_ct_award_delete_menu_link('menu-site-manager-vocabularies', 'admin/structure/taxonomy/uw_departments');
}

/**
 * Implements hook_enable().
 */
function uw_ct_award_enable() {
  watchdog('uw_ct_award', 'enable function executing');

  // Enable the view.
  // Read in any existing disabled views.
  $views_status = variable_get('views_defaults', array());

  // Remove ours from the list.
  unset($views_status['manage_undergrad_awards']);
  unset($views_status['uw_undergrad_award_mass_publish']);
  unset($views_status['uw_undergrad_award_page']);
  unset($views_status['uw_undergrad_award_report']);
  unset($views_status['uw_undergrad_award_search_block']);
  unset($views_status['uwaterloo_services_undergrad_awards']);

  // Reset the variable with the new list.
  variable_set('views_defaults', $views_status);
  if (function_exists('views_invalidate_cache')) {
    // Clear the views cache.
    views_invalidate_cache();
  }

  // The landing page with alias awards/database will create automatically if it
  // is not exist.
  _uw_ct_award_create_landing_page();
}

/**
 * Implements hook_update_dependencies().
 */
function uw_ct_award_update_dependencies() {
  // Update hook 7200 must run after pathauto's update hook 7006
  // as 7200 creates data in a table that 7006 creates.
  $dependencies['uw_ct_award'][7200] = array(
    'pathauto' => 7006,
  );
  return $dependencies;
}

/**
 * Implements hook_update_N().
 */

/**
 * Enable modules.
 */
function uw_ct_award_update_7001() {
  $modules = array(
    'taxonomy_formatter',
    'field_permissions',
    'views_data_export',
    'views_bulk_operations',
  );
  module_enable($modules);
}

/**
 * Landing page.
 *
 * The landing page with alias awards/database will create automatically
 * when uw_ct_award module has been enabled, but the landing page is not exist.
 */
function uw_ct_award_update_7200() {
  if (module_exists('uw_ct_award')) {
    _uw_ct_award_create_landing_page();
  }
}

/**
 * Redo hook_update_7001 to ensure all necessary updates fires.
 */
function uw_ct_award_update_7201() {
  $modules = array(
    'taxonomy_formatter',
    'field_permissions',
    'views_data_export',
    'views_bulk_operations',
  );
  module_enable($modules);
}

/**
 * Delete the menu link admin/config/system/awards/search_result_header.
 */
function uw_ct_award_update_7202() {
  menu_link_delete(NULL, 'admin/config/system/awards/search_result_header');
}

/**
 * Set variable uw_ct_award_site_type for the existing sites.
 */
function uw_ct_award_update_7203() {
  $base_path = base_path();
  if ($base_path == '/student-awards-financial-aid/') {
    variable_set('uw_ct_award_site_type', 'student-awards-financial-aid');
  }
  elseif ($base_path == '/graduate-studies/' || $base_path == '/graduate-studies-postdoctoral-affairs/') {
    variable_set('uw_ct_award_site_type', 'graduate-studies');
  }
  else {
    variable_set('uw_ct_award_site_type', 'other');
  }
}

/**
 * Add vocabulary award contact.
 */
function uw_ct_award_update_7204() {
  // Check if Award contact is existing.
  $vid = db_select("taxonomy_vocabulary", "tv")
    ->fields("tv", array("vid"))
    ->condition("machine_name", 'undergrad_award_contact')
    ->execute()
    ->fetchField();

  // Create Award contact vocabulary if not existing.
  if (!$vid) {
    taxonomy_vocabulary_save((object) array(
      'name' => 'Award contact',
      'machine_name' => 'undergrad_award_contact',
    ));
  }
}

/**
 * Rename AHS to "Faculty of Health".
 */
function uw_ct_award_update_7205() {
  $terms = taxonomy_get_term_by_name('Applied Health Science');
  foreach ($terms as $term) {
    // Terms do not include "Faculty of".
    $term->name = 'Health';
    taxonomy_term_save($term);
  }
}
